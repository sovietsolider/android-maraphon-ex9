package com.example.maraphon_ex8

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlin.random.Random


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    var numToExpect: Int = 0

    fun getRandomIntExpectOne(num: Int): Int {
        var n = Random.nextInt(0, 4)
        while(num == n)
            n = Random.nextInt(0, 4)
        return n
    }

    fun onButtonClick(view: View) {
        val image: ImageView = findViewById(R.id.imageView)
        val rnd = getRandomIntExpectOne(numToExpect)
        numToExpect = rnd

        when(rnd){
            0 -> image.setImageResource(R.drawable.img1)
            1 -> image.setImageResource(R.drawable.img2)
            2 -> image.setImageResource(R.drawable.img3)
            3 -> image.setImageResource(R.drawable.img4)
        }
    }

}